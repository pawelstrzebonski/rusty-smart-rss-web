use serde::{Deserialize, Serialize};
use std::fs;
use std::sync::{Arc, Mutex};
use tiny_http::{Header, Method, Request, Response, Server, StatusCode};
extern crate dirs;
use percent_encoding::{percent_decode_str, utf8_percent_encode, CONTROLS};
use std::collections::HashMap;
use std::thread;
use std::time::Duration;

#[macro_use]
extern crate log;
/// Context structure for web UI templating
#[derive(Serialize, Deserialize)]
struct Context {
    items: Vec<LocalItem>,           // List of RSS feed items
    limit: usize,                    // # of RSS items to diplay per page
    feeds: Vec<LocalFeed>,           // List of subscribed RSS feeds
    wordscores: Vec<LocalWordScore>, // List of scored words
}

/// Local struct for word-score pair
#[derive(Serialize, Deserialize)]
struct LocalWordScore {
    word: String, // Word
    score: f64,   // Word's score
}

/// Local struct for RSS feed items
#[derive(Serialize, Deserialize, Clone)]
struct LocalItem {
    title: String,   // Item title
    url: String,     // URL the RSS item links to
    summary: String, // Item summary text
    score: f64,      // Classifier's score of the RSS item
}

/// Convert list of back-end item structs into list of front-end item structs
fn tolocalitems(items: &[libsmartrssrust::MyItem]) -> Vec<LocalItem> {
    let mut displayitems = vec![];
    for item in items {
        displayitems.push(LocalItem {
            title: item.title.clone(),
            url: item.url.clone(),
            summary: item.summary.clone(),
            score: item.score,
        });
    }
    displayitems
}

/// Local struct for RSS feeds
#[derive(Serialize, Deserialize, Clone)]
struct LocalFeed {
    url: String, // URL of the RSS feed
}

/// Convert list of back-end feed structs into list of front-end feed structs
fn tolocalfeeds(feeds: &[libsmartrssrust::MyFeed]) -> Vec<LocalFeed> {
    let mut displayfeeds = vec![];
    for feed in feeds {
        displayfeeds.push(LocalFeed {
            url: feed.url.clone(),
        });
    }
    displayfeeds
}

fn parse_key_value(instr: &str) -> Vec<(String, String)> {
    let mut kvs = vec![];
    // Split on & into individual K/V pairs
    for substr in instr.split('&') {
        // Split on =
        let subsubstr: Vec<_> = substr.split('=').collect();
        // Iff we have 2 substrings split by =, we have a K/V pair
        if subsubstr.len() == 2 {
            kvs.push((subsubstr[0].into(), subsubstr[1].into()));
        }
    }
    kvs
}

/// Given a request return the key-value data from the body as a hashmap
fn get_form(request: &mut Request) -> HashMap<String, String> {
    match request.method() {
        Method::Post => {
            let mut content = String::new();
            request.as_reader().read_to_string(&mut content).unwrap();
            let res = parse_key_value(&content);
            let mut hm = HashMap::new();
            for (k, v) in res {
                hm.insert(
                    percent_decode_str(&k).decode_utf8().unwrap().to_string(),
                    percent_decode_str(&v).decode_utf8().unwrap().to_string(),
                );
            }
            hm
        }
        _ => HashMap::new(),
    }
}

/// Application state structure
struct AppState {
    backend: libsmartrssrust::LibSmartRSS, // Backend, courtesy of libsmartrssrust
    limit: usize,                          // Max # of feed items to show per page
    items: Vec<LocalItem>,                 // List of feed items to show
    feeds: Vec<LocalFeed>,                 // List of currently subscribed feeds
}

use signal_hook::{
    consts::SIGHUP, consts::SIGINT, consts::SIGQUIT, consts::SIGTERM, iterator::Signals,
};

fn main() {
    // Setup logging
    simple_logger::init_with_env().unwrap();
    // Get command line arguments
    let mut pargs = pico_args::Arguments::from_env();
    // Set "pure" mode (no read/write to file)
    let ispure = pargs.contains(["-p", "--pure"]);
    // Set name of database file
    let dbname = pargs
        .value_from_str("--db")
        .unwrap_or_else(|_| "smartrss.bin".to_string());
    // Set port of server
    let port = pargs.value_from_str("--port").unwrap_or(8000);
    // Set initial # of items to display
    let limit = pargs.value_from_str("--limit").unwrap_or(50);

    // Setup config directory for database
    let confdir = dirs::config_dir().unwrap().join("smartrssrust");
    fs::create_dir_all(&confdir).unwrap();
    let dbfile = if ispure {
        "".to_string()
    } else {
        confdir.join(dbname).into_os_string().into_string().unwrap()
    };
    // Setup backend
    info!("Setting up back-end");
    let backend = libsmartrssrust::setup(&dbfile).unwrap();
    info!("Back-end set up");
    // Load initial feed data from backend
    let items = tolocalitems(&backend.items_get_unseen_limited(&(limit as i64)).unwrap());
    let feeds = tolocalfeeds(&backend.feeds_get().unwrap());
    // Setup the application state
    let state = Arc::new(Mutex::new(AppState {
        backend: backend,
        limit: limit,
        items: items,
        feeds: feeds,
    }));
    // Spawn automatic update thread
    let autoupdate = Arc::clone(&state);
    thread::spawn(move || loop {
        {
            let mut state = autoupdate.lock().unwrap();
            state.backend.feeds_update();
        }
        thread::sleep(Duration::from_secs(600));
    });

    // Spawn automatic save thread
    let autosave = Arc::clone(&state);
    thread::spawn(move || loop {
        {
            let mut state = autosave.lock().unwrap();
            state.backend.save_db();
        }
        thread::sleep(Duration::from_secs(3600));
    });
    // Setup save-on-exit thread
    let mut signals = Signals::new([SIGTERM, SIGINT, SIGQUIT, SIGHUP]).unwrap();
    let finalsave = Arc::clone(&state);
    thread::spawn(move || {
        if let Some(sig) = signals.forever().next() {
            println!("Received signal {:?}", sig);
            let mut state = finalsave.lock().unwrap();
            state.backend.save_db();
            println!("Saved backend");
            std::process::exit(0);
        }
    });
    // Incorporate favicon into the binary
    let icon = include_bytes!("../assets/favicon.ico");

    // Load templates
    let mut tt = tinytemplate::TinyTemplate::new();
    tt.add_template("index", include_str!("../templates/main.hbs"))
        .unwrap();
    tt.add_template("wordscores", include_str!("../templates/wordscores.hbs"))
        .unwrap();

    // Define some commonly used headers
    let htmlheader = Header::from_bytes(&b"Content-Type"[..], &b"text/html"[..]).unwrap();
    let homeheader = Header::from_bytes(&b"Location"[..], &b"/"[..]).unwrap();

    // Launch/create the server
    let server = Server::http(format!("0.0.0.0:{}", port)).unwrap();

    // Handle each request
    for mut request in server.incoming_requests() {
        // Parse the body of the request
        let form = get_form(&mut request);
        // Match request URL and method to corresponding code to handle it (diy router)
        let response = match request.url() {
            "/" => match request.method() {
                Method::Get => {
                    let state = state.lock().unwrap();
                    let context = Context {
                        items: state.items.clone(),
                        limit: state.limit,
                        feeds: state.feeds.clone(),
                        wordscores: vec![],
                    };
                    Response::from_string(tt.render("index", &context).unwrap())
                        .with_header(htmlheader.clone())
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/wordscores" => match request.method() {
                Method::Get => {
                    let mut state = state.lock().unwrap();
                    let context = Context {
                        items: vec![],
                        limit: state.limit,
                        feeds: vec![],
                        wordscores: state
                            .backend
                            .word_scores_get()
                            .unwrap()
                            .iter()
                            .map(|ws| LocalWordScore {
                                word: ws.word.clone().into(),
                                score: ws.score,
                            })
                            .collect(),
                    };
                    Response::from_string(tt.render("wordscores", &context).unwrap())
                        .with_header(htmlheader.clone())
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/like" => match request.method() {
                Method::Post => {
                    let url = &form["url"];
                    info!("Liking {}", url);
                    let mut state = state.lock().unwrap();
                    state.backend.item_like(url);
                    Response::from_string("")
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/dislike" => match request.method() {
                Method::Post => {
                    let url = &form["url"];
                    info!("Disliking {}", url);
                    let mut state = state.lock().unwrap();
                    state.backend.item_dislike(url);
                    Response::from_string("")
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/open" => match request.method() {
                Method::Post => {
                    let url = &form["url"];
                    info!("Opening {}", url);
                    let mut state = state.lock().unwrap();
                    state.backend.item_mark_opened(url);
                    Response::from_string("")
                        .with_header(
                            Header::from_bytes(
                                &b"Location"[..],
                                utf8_percent_encode(url, CONTROLS).to_string(),
                            )
                            .unwrap(),
                        )
                        .with_status_code(StatusCode(302))
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/add" => match request.method() {
                Method::Post => {
                    let url = &form["url"];
                    let num = &form["num"];
                    info!("Adding {}", url);
                    let mut state = state.lock().unwrap();
                    state.backend.feed_add(url, &(num.parse().unwrap()));
                    state.feeds = tolocalfeeds(&state.backend.feeds_get().unwrap());
                    Response::from_string("")
                        .with_header(homeheader.clone())
                        .with_status_code(StatusCode(302))
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/remove" => match request.method() {
                Method::Post => {
                    let url = &form["url"];
                    info!("Removing {}", url);
                    let mut state = state.lock().unwrap();
                    state.backend.feed_remove(url);
                    state.feeds = tolocalfeeds(&state.backend.feeds_get().unwrap());
                    Response::from_string("")
                        .with_header(homeheader.clone())
                        .with_status_code(StatusCode(302))
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/setitemnum" => match request.method() {
                Method::Post => {
                    let mut state = state.lock().unwrap();
                    let num = &form["num"];
                    state.limit = num.parse().unwrap();
                    // Get new set of items
                    state.items = tolocalitems(
                        &state
                            .backend
                            .items_get_unseen_limited(&(state.limit as i64))
                            .unwrap(),
                    );
                    Response::from_string("")
                        .with_header(homeheader.clone())
                        .with_status_code(StatusCode(302))
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/nextpage" => match request.method() {
                Method::Post => {
                    let mut state = state.lock().unwrap();
                    // Mark items as seen
                    for i in 0..state.items.len() {
                        let url = state.items[i].url.clone();
                        state.backend.item_mark_seen(&url);
                    }
                    // Get new set of items
                    state.items = tolocalitems(
                        &state
                            .backend
                            .items_get_unseen_limited(&(state.limit as i64))
                            .unwrap(),
                    );
                    Response::from_string("")
                        .with_header(homeheader.clone())
                        .with_status_code(StatusCode(302))
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/favicon.ico" => match request.method() {
                Method::Get => tiny_http::Response::from_data(icon.to_vec()),
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            "/stop" => match request.method() {
                Method::Post => {
                    info!("Recieved Stop request, exiting...");
                    let mut state = state.lock().unwrap();
                    state.backend.save_db();
                    println!("Saved backend");
                    std::process::exit(0);
                }
                _ => Response::from_string("bad method").with_status_code(StatusCode(405)),
            },
            _ => Response::from_string("not found").with_status_code(StatusCode(404)),
        };
        request.respond(response).unwrap();
    }
}
