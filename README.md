# Rusty Smart-RSS Web

A machine learning enhanced RSS feed manager (written in Rust). Unlike the parallel project [Rusty Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss) (which has a native desktop UI), this application runs a local web-server that allows you to access the web-UI in your web-browser of choice. This application is built on the [`libsmartrssrust`](https://gitlab.com/pawelstrzebonski/libsmartrssrust) library.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/rusty-smart-rss-web/).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Web UI](docs/mainui.png)

## Usage/Installation

This repository's code can be built using the standard Rust toolset, namely `cargo`, but may also require installation of build dependencies from your systems package respositories. By default, the web UI can be accessed in you browser of choice at "localhost:8000".
