{ pkgs ? import <nixpkgs> {} }:
with pkgs;

rustPlatform.buildRustPackage rec {
	name = "rustysmartrssweb";
	nativeBuildInputs = [pkg-config];
	buildInputs = [openssl];
	rev = "28685cd2e1324fa8ff3dbb11133f623bce512bd3";
	src = fetchFromGitLab {
		inherit rev;
		owner = "pawelstrzebonski";
		repo = "rusty-smart-rss-web";
		sha256 = "sha256-TFgAirY5INnzh2oMk6BAf/GqNe2hVJfySTXzzpKKGuo=";
	};
	cargoLock = {
		lockFile = ./Cargo.lock;
		allowBuiltinFetchGit = true;
	};
	doCheck = false;
	meta = with lib; {
		description = "A machine learning enhanced RSS feed manager, written in Rust.";
		homepage = "https://gitlab.com/pawelstrzebonski/rusty-smart-rss-web";
		license = licenses.agpl3Plus;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
