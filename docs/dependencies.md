# Dependencies

This application makes use of the following Rust crates:

* [`libsmartrssrust`](https://gitlab.com/pawelstrzebonski/libsmartrssrust): Back-end library
* [`tiny-http`](https://github.com/tiny-http/tiny-http): Web server library
* [`tinytemplate`](https://github.com/bheisler/tinytemplate): Templating
* [`serde`](https://serde.rs/): Serialization/deserialization framework
* [`dirs`](https://github.com/dirs-dev/dirs-rs): Query user's configurations directory
* [`simple_logger`](https://github.com/borntyping/rust-simple_logger)/[`log`](https://github.com/rust-lang/log): Logging of errors/events in the backend for debugging purposes
* [`pico-args`](https://github.com/RazrFalcon/pico-args): Command line argument parsing
* [`signal-hook`](https://github.com/vorner/signal-hook): Handling application closing signals
* [`regex`](https://github.com/rust-lang/regex): Regex parsing of HTTP request body
* [`percent-encoding`](https://github.com/servo/rust-url/tree/HEAD/percent_encoding): Decoding percent encoding of HTTP request body
* [`lazy-static`](https://github.com/rust-lang-nursery/lazy-static.rs): Lazy static evaluation (of Regex to avoid repeated compilation)
