# (Rusty) Smart-RSS Web

(Rusty) Smart-RSS Web is a machine learning enhanced RSS feed manager, written in Rust. It is a Rust equivalent to the prior [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web) and a parallel front-end project to [Rusty Smart-RSS](https://gitlab.com/pawelstrzebonski/rusty-smart-rss), both of which use the back-end library [libsmartrssrust](https://gitlab.com/pawelstrzebonski/libsmartrssrust).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Web UI](mainui.png) 

Main web UI

![Word scores page](wordscores.png)

Page listing scores of words
