# TODOs

The following is a shopping list of potential improvements (in no particular order):

* Better GUI (expose more application settings, nicer presentation)
* General code/project improvements (tests, CI, build documentation from source, decrease build dependency count, etc)
