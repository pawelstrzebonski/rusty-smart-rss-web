# Design Document

## Files

This application is four primary files:

* `src/main.rs`
* `templates/main.hbs`
* `templates/wordscores.hbs`
* `assets/favicon.ico`

The `main.rs` file loads and configures the back-end (`libsmartrssrust`) and handles the web server (`tiny-http`). The webpage served by this server acts as the front-end/UI.

The `main.hbs` and `wordscores.hbs` files are the UI template, processed by `tinytemplate`.

The `favicon.ico` is simply the favicon served along with the UI.
